## C42 Rest Web Server

This is an example of the C42 Rest server, it interfaces with a shareplan server via the c42_rest module, this allows you to build a web application without worrying about
having the app live on the same server as shareplan. It currently provides a same / similar api endpoint as the official api, you can currently do the following things with this:

- authenticate your account
- get information about users / your user account
- get information about a plan
- upload files to a plan 

## Installing the server

```
# clone the repo
git clone https://vanntastic@bitbucket.org/vanntastic/c42-rest-server.git

# install dependencies
npm install

# start the server
node server.js
```



